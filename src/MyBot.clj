(ns MyBot
  (:require [clojure.java.io :as clj.io]
            [hlt.networking :as io]
            [hlt.game-map :refer [*player-id* *map-size* *bot-name*
                                  *owner-ships* *ships* *planets*]]
            [hlt.utils :as utils]
            [hlt.entity :as e]
            [hlt.math :as math]
            [hlt.navigation :as navigation])
  (:import (java.io PrintWriter))
  (:gen-class))

(defmacro with-updated-map
  [& body]
  `(let [m# (io/read-map)]
     (binding [*owner-ships* (:owner-ships m#)
               *ships* (:ships m#)
               *planets* (:planets m#)]
       ~@body)))

(def my-bot-name "J")
(def num-seeders 30)
(defmacro initialize-game
  [& body]
  `(let [prelude# (io/read-prelude)
         bot-name# (str my-bot-name "-" (:player-id prelude#))]
     (with-open [logger# (clj.io/writer (str bot-name# ".log"))]
       (binding [utils/*logger* logger#
                 *bot-name* bot-name#
                 *player-id* (:player-id prelude#)
                 *map-size* (:map-size prelude#)]
         (try
           (with-updated-map ~@body)
           (catch Throwable t#
             (with-open [pw# (PrintWriter. utils/*logger*)]
               (.printStackTrace t# pw#))
             (throw t#)))))))

(defn unclaimed
  [planet]
  (nil? (:owner-id planet)))

(defn owned-by-us
  [planet]
  (= (:owner-id planet) *player-id*))

(defn to-target
  [planet]
  (or (nil? (:owner-id planet))
      (not (owned-by-us planet))
      (e/any-remaining-docking-spots? planet)))

(defn valid-colony
  [planet]
  (or (unclaimed planet)
      (and (= (:owner-id planet) *player-id*)
           (e/any-remaining-docking-spots? planet))))

(defn dock-cmd
  [ship planet]
  (if (e/within-docking-range? ship planet)
    (e/dock-move ship planet)
    (navigation/navigate-to-dock ship planet)))

(defn get-closest-thing
  [ship planets]
  (when (seq planets)
    (apply min-key #(math/distance-between ship %) planets)))

(defn compute-other-move
  [enemy-docks ship]
  (when enemy-docks
    (when-let [closest-dock (get-closest-thing ship enemy-docks)]
      (navigation/navigate-to ship closest-dock))))

(defn compute-seeder-move
  [enemy-docks ship]
  (if-not (not= (-> ship :docking :status) :undocked)
    (let [
          unclaimed-planets (->> (vals *planets*)
                                 (filter unclaimed))
          ;; Closest unclaimed
          ;; closest-planet (as-> unclaimed-planets p
          ;;                  (filter #(< (math/distance-between ship %) (/ (first *map-size*) 8)) p)
          ;;                  (get-closest-thing ship p))
          closest-planet nil
          ;; Closest period
          closest-planet (or closest-planet
                             (->> (vals *planets*)
                                  (filter to-target)
                                  (get-closest-thing ship)))]
      (if (and closest-planet
               (or (unclaimed closest-planet)
                   (owned-by-us closest-planet)))
        (dock-cmd ship closest-planet)
        (compute-other-move enemy-docks ship)))))

(defn -main
  [& args]
  (initialize-game
   (io/send-done-initialized)
   (doseq [turn (iterate inc 1)]
     (with-updated-map
       (utils/log "=========== Turn" turn "===========")
       (let [our-ships (vals (get *owner-ships* *player-id*))
             enemy-docks (->>
                          *ships*
                          (vals)
                          (filter #(not= (:owner-id %) *player-id*))
                          (filter #(or (= (-> % :docking :status) :docking)
                                       (= (-> % :docking :status) :docked))))
             move-gatherers (keep (partial compute-seeder-move enemy-docks) our-ships)

             ;; IF REENABLING THIS, TAKE ONLY WHAT WE NEED ABOVE
             ;; move-others (keep (partial compute-other-move enemy-docks) (drop num-seeders our-ships))
             move-others '()
             moves (concat move-gatherers move-others)]
         (io/send-moves moves))))))
