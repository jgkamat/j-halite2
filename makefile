
all:
	lein uberjar

run:
	./run_game.sh
r: run


clean:
	lein clean
	rm -f *.log
	rm -f *.hlt
	rm -f *.zip

dist:
	zip -r bot.zip *
